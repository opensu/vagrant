#!/bin/bash

set -euxo pipefail
export USERNAME=$(id -un 1000)

# curl -fsSL https://mirrors.aliyun.com/docker-ce/linux/ubuntu/gpg
# curl -fsSL https://download.docker.com/linux/ubuntu/gpg
tee /tmp/gpg-docker >/dev/null<<EOF
-----BEGIN PGP PUBLIC KEY BLOCK-----

mQINBFit2ioBEADhWpZ8/wvZ6hUTiXOwQHXMAlaFHcPH9hAtr4F1y2+OYdbtMuth
lqqwp028AqyY+PRfVMtSYMbjuQuu5byyKR01BbqYhuS3jtqQmljZ/bJvXqnmiVXh
38UuLa+z077PxyxQhu5BbqntTPQMfiyqEiU+BKbq2WmANUKQf+1AmZY/IruOXbnq
L4C1+gJ8vfmXQt99npCaxEjaNRVYfOS8QcixNzHUYnb6emjlANyEVlZzeqo7XKl7
UrwV5inawTSzWNvtjEjj4nJL8NsLwscpLPQUhTQ+7BbQXAwAmeHCUTQIvvWXqw0N
cmhh4HgeQscQHYgOJjjDVfoY5MucvglbIgCqfzAHW9jxmRL4qbMZj+b1XoePEtht
ku4bIQN1X5P07fNWzlgaRL5Z4POXDDZTlIQ/El58j9kp4bnWRCJW0lya+f8ocodo
vZZ+Doi+fy4D5ZGrL4XEcIQP/Lv5uFyf+kQtl/94VFYVJOleAv8W92KdgDkhTcTD
G7c0tIkVEKNUq48b3aQ64NOZQW7fVjfoKwEZdOqPE72Pa45jrZzvUFxSpdiNk2tZ
XYukHjlxxEgBdC/J3cMMNRE1F4NCA3ApfV1Y7/hTeOnmDuDYwr9/obA8t016Yljj
q5rdkywPf4JF8mXUW5eCN1vAFHxeg9ZWemhBtQmGxXnw9M+z6hWwc6ahmwARAQAB
tCtEb2NrZXIgUmVsZWFzZSAoQ0UgZGViKSA8ZG9ja2VyQGRvY2tlci5jb20+iQI3
BBMBCgAhBQJYrefAAhsvBQsJCAcDBRUKCQgLBRYCAwEAAh4BAheAAAoJEI2BgDwO
v82IsskP/iQZo68flDQmNvn8X5XTd6RRaUH33kXYXquT6NkHJciS7E2gTJmqvMqd
tI4mNYHCSEYxI5qrcYV5YqX9P6+Ko+vozo4nseUQLPH/ATQ4qL0Zok+1jkag3Lgk
jonyUf9bwtWxFp05HC3GMHPhhcUSexCxQLQvnFWXD2sWLKivHp2fT8QbRGeZ+d3m
6fqcd5Fu7pxsqm0EUDK5NL+nPIgYhN+auTrhgzhK1CShfGccM/wfRlei9Utz6p9P
XRKIlWnXtT4qNGZNTN0tR+NLG/6Bqd8OYBaFAUcue/w1VW6JQ2VGYZHnZu9S8LMc
FYBa5Ig9PxwGQOgq6RDKDbV+PqTQT5EFMeR1mrjckk4DQJjbxeMZbiNMG5kGECA8
g383P3elhn03WGbEEa4MNc3Z4+7c236QI3xWJfNPdUbXRaAwhy/6rTSFbzwKB0Jm
ebwzQfwjQY6f55MiI/RqDCyuPj3r3jyVRkK86pQKBAJwFHyqj9KaKXMZjfVnowLh
9svIGfNbGHpucATqREvUHuQbNnqkCx8VVhtYkhDb9fEP2xBu5VvHbR+3nfVhMut5
G34Ct5RS7Jt6LIfFdtcn8CaSas/l1HbiGeRgc70X/9aYx/V/CEJv0lIe8gP6uDoW
FPIZ7d6vH+Vro6xuWEGiuMaiznap2KhZmpkgfupyFmplh0s6knymuQINBFit2ioB
EADneL9S9m4vhU3blaRjVUUyJ7b/qTjcSylvCH5XUE6R2k+ckEZjfAMZPLpO+/tF
M2JIJMD4SifKuS3xck9KtZGCufGmcwiLQRzeHF7vJUKrLD5RTkNi23ydvWZgPjtx
Q+DTT1Zcn7BrQFY6FgnRoUVIxwtdw1bMY/89rsFgS5wwuMESd3Q2RYgb7EOFOpnu
w6da7WakWf4IhnF5nsNYGDVaIHzpiqCl+uTbf1epCjrOlIzkZ3Z3Yk5CM/TiFzPk
z2lLz89cpD8U+NtCsfagWWfjd2U3jDapgH+7nQnCEWpROtzaKHG6lA3pXdix5zG8
eRc6/0IbUSWvfjKxLLPfNeCS2pCL3IeEI5nothEEYdQH6szpLog79xB9dVnJyKJb
VfxXnseoYqVrRz2VVbUI5Blwm6B40E3eGVfUQWiux54DspyVMMk41Mx7QJ3iynIa
1N4ZAqVMAEruyXTRTxc9XW0tYhDMA/1GYvz0EmFpm8LzTHA6sFVtPm/ZlNCX6P1X
zJwrv7DSQKD6GGlBQUX+OeEJ8tTkkf8QTJSPUdh8P8YxDFS5EOGAvhhpMBYD42kQ
pqXjEC+XcycTvGI7impgv9PDY1RCC1zkBjKPa120rNhv/hkVk/YhuGoajoHyy4h7
ZQopdcMtpN2dgmhEegny9JCSwxfQmQ0zK0g7m6SHiKMwjwARAQABiQQ+BBgBCAAJ
BQJYrdoqAhsCAikJEI2BgDwOv82IwV0gBBkBCAAGBQJYrdoqAAoJEH6gqcPyc/zY
1WAP/2wJ+R0gE6qsce3rjaIz58PJmc8goKrir5hnElWhPgbq7cYIsW5qiFyLhkdp
YcMmhD9mRiPpQn6Ya2w3e3B8zfIVKipbMBnke/ytZ9M7qHmDCcjoiSmwEXN3wKYI
mD9VHONsl/CG1rU9Isw1jtB5g1YxuBA7M/m36XN6x2u+NtNMDB9P56yc4gfsZVES
KA9v+yY2/l45L8d/WUkUi0YXomn6hyBGI7JrBLq0CX37GEYP6O9rrKipfz73XfO7
JIGzOKZlljb/D9RX/g7nRbCn+3EtH7xnk+TK/50euEKw8SMUg147sJTcpQmv6UzZ
cM4JgL0HbHVCojV4C/plELwMddALOFeYQzTif6sMRPf+3DSj8frbInjChC3yOLy0
6br92KFom17EIj2CAcoeq7UPhi2oouYBwPxh5ytdehJkoo+sN7RIWua6P2WSmon5
U888cSylXC0+ADFdgLX9K2zrDVYUG1vo8CX0vzxFBaHwN6Px26fhIT1/hYUHQR1z
VfNDcyQmXqkOnZvvoMfz/Q0s9BhFJ/zU6AgQbIZE/hm1spsfgvtsD1frZfygXJ9f
irP+MSAI80xHSf91qSRZOj4Pl3ZJNbq4yYxv0b1pkMqeGdjdCYhLU+LZ4wbQmpCk
SVe2prlLureigXtmZfkqevRz7FrIZiu9ky8wnCAPwC7/zmS18rgP/17bOtL4/iIz
QhxAAoAMWVrGyJivSkjhSGx1uCojsWfsTAm11P7jsruIL61ZzMUVE2aM3Pmj5G+W
9AcZ58Em+1WsVnAXdUR//bMmhyr8wL/G1YO1V3JEJTRdxsSxdYa4deGBBY/Adpsw
24jxhOJR+lsJpqIUeb999+R8euDhRHG9eFO7DRu6weatUJ6suupoDTRWtr/4yGqe
dKxV3qQhNLSnaAzqW/1nA3iUB4k7kCaKZxhdhDbClf9P37qaRW467BLCVO/coL3y
Vm50dwdrNtKpMBh3ZpbB1uJvgi9mXtyBOMJ3v8RZeDzFiG8HdCtg9RvIt/AIFoHR
H3S+U79NT6i0KPzLImDfs8T7RlpyuMc4Ufs8ggyg9v3Ae6cN3eQyxcK3w0cbBwsh
/nQNfsA6uu+9H7NhbehBMhYnpNZyrHzCmzyXkauwRAqoCbGCNykTRwsur9gS41TQ
M8ssD1jFheOJf3hODnkKU+HKjvMROl1DK7zdmLdNzA1cvtZH/nCC9KPj1z8QC47S
xx+dTZSx4ONAhwbS/LN3PoKtn8LPjY9NP9uDWI+TWYquS2U+KHDrBDlsgozDbs/O
jCxcpDzNmXpWQHEtHU7649OXHP7UeNST1mCUCH5qdank0V1iejF6/CfTFU4MfcrG
YT90qFF93M3v01BbxP+EIY2/9tiIPbrd
=0YYh
-----END PGP PUBLIC KEY BLOCK-----
EOF

# curl -fsSL https://pkgs.k8s.io/core:/stable:/v${KUBERNETES_VERSION}/deb/Release.key
tee /tmp/gpg-kubernetes >/dev/null<<EOF
-----BEGIN PGP PUBLIC KEY BLOCK-----
Version: GnuPG v2.0.15 (GNU/Linux)

mQENBGMHoXcBCADukGOEQyleViOgtkMVa7hKifP6POCTh+98xNW4TfHK/nBJN2sm
u4XaiUmtB9UuGt9jl8VxQg4hOMRf40coIwHsNwtSrc2R9v5Kgpvcv537QVIigVHH
WMNvXeoZkkoDIUljvbCEDWaEhS9R5OMYKd4AaJ+f1c8OELhEcV2dAQLLyjtnEaF/
qmREN+3Y9+5VcRZvQHeyBxCG+hdUGE740ixgnY2gSqZ/J4YeQntQ6pMUEhT6pbaE
10q2HUierj/im0V+ZUdCh46Lk/Rdfa5ZKlqYOiA2iN1coDPIdyqKavcdfPqSraKF
Lan2KLcZcgTxP+0+HfzKefvGEnZa11civbe9ABEBAAG0PmlzdjprdWJlcm5ldGVz
IE9CUyBQcm9qZWN0IDxpc3Y6a3ViZXJuZXRlc0BidWlsZC5vcGVuc3VzZS5vcmc+
iQE+BBMBCAAoBQJjB6F3AhsDBQkEHrAABgsJCAcDAgYVCAIJCgsEFgIDAQIeAQIX
gAAKCRAjRlTamilkNhnRCADud9iv+2CUtJGyZhhdzzd55wRKvHGmSY4eIAEKChmf
1+BHwFnzBzbdNtnglY2xSATqKIWikzXI1stAwi8qR0dK32CS+ofMS6OUklm26Yd1
jBWFg4LCCh8S21GLcuudHtW9QNCCjlByS4gyEJ+eYTOo2dWp88NWEzVXIKRtfLHV
myHJnt2QLmWOeYTgmCzpeT8onl2Lp19bryRGla+Ms0AmlCltPn8j+hPeADDtR2bv
7cTLDi/nA46u3SLV1P6yjC1ejOOswtgxppTxvLgYniS22aSnoqm47l111zZiZKJ5
bCm1Th6qJFJwOrGEOu3aV1iKaQmN2k4G2DixsHFAU3ZeiQIcBBMBAgAGBQJjB6F3
AAoJEM8Lkoze1k873TQP/0t2F/jltLRQMG7VCLw7+ps5JCW5FIqu/S2i9gSdNA0E
42u+LyxjG3YxmVoVRMsxeu4kErxr8bLcA4p71W/nKeqwF9VLuXKirsBC7z2syFiL
Ndl0ARnC3ENwuMVlSCwJO0MM5NiJuLOqOGYyD1XzSfnCzkXN0JGA/bfPRS5mPfoW
0OHIRZFhqE7ED6wyWpHIKT8rXkESFwszUwW/D7o1HagX7+duLt8WkrohGbxTJ215
YanOKSqyKd+6YGzDNUoGuMNPZJ5wTrThOkTzEFZ4HjmQ16w5xmcUISnCZd4nhsbS
qN/UyV9Vu3lnkautS15E4CcjP1RRzSkT0jka62vPtAzw+PiGryM1F7svuRaEnJD5
GXzj9RCUaR6vtFVvqqo4fvbA99k4XXj+dFAXW0TRZ/g2QMePW9cdWielcr+vHF4Z
2EnsAmdvF7r5e2JCOU3N8OUodebU6ws4VgRVG9gptQgfMR0vciBbNDG2Xuk1WDk1
qtscbfm5FVL36o7dkjA0x+TYCtqZIr4x3mmfAYFUqzxpfyXbSHqUJR2CoWxlyz72
XnJ7UEo/0UbgzGzscxLPDyJHMM5Dn/Ni9FVTVKlALHnFOYYSTluoYACF1DMt7NJ3
oyA0MELL0JQzEinixqxpZ1taOmVR/8pQVrqstqwqsp3RABaeZ80JbigUC29zJUVf
=F4EX
-----END PGP PUBLIC KEY BLOCK-----
EOF

# define function
function timezone_change {
	timedatectl set-timezone Asia/Shanghai
}

function passwd_root {
	# 更新 root 密码
	(echo ${ROOT_PASS}; echo ${ROOT_PASS}) \
		| passwd root
	
	# 更新文件 /etc/ssh/sshd_config
	sed -i /etc/ssh/sshd_config \
		-e '/PasswordAuthentication /{s+#++;s+no+yes+}'
	if egrep -q '^(#|)PermitRootLogin' /etc/ssh/sshd_config; then
		sed -i /etc/ssh/sshd_config \
			-e '/^#PermitRootLogin/{s+#++;s+ .*+ yes+}' \
			-e '/^PermitRootLogin/{s+#++;s+ .*+ yes+}' 
	fi

	# 重启 sshd 服务
	systemctl restart sshd

	# 生成 ssh 客户端配置文件
	tee /etc/ssh/ssh_config.d/k8s.conf >/dev/null<<-EOF
		Host *
			StrictHostKeyChecking no
			UserKnownHostsFile /dev/null
			LogLevel QUIET
	EOF
}

function service_restart_accept {
	# Which services should be restarted
	if [ "$(lsb_release -cs)" = "jammy" ]; then
		sed -i /etc/needrestart/needrestart.conf \
			-e '/nrconf{restart}/{s+i+a+;s+#++}'
	fi
}

function dns_setting {
	sed -i /etc/systemd/resolved.conf \
  		-e '/^#DNS=/{s/=.*/='"${DNS_SERVERS}"'/;s/#//}'
	systemctl restart systemd-resolved
	ln -sf /run/systemd/resolve/resolv.conf /etc/resolv.conf

	tee -a /etc/netplan/50-vagrant.yaml >/dev/null<<-EOF
		      nameservers:
		        addresses: [$(echo ${DNS_SERVERS} | sed 's+ +,+g')]
	EOF
}

function apt_mirror {
	cat /tmp/gpg-docker | apt-key add -

	if ! curl --connect-timeout 2 google.com &>/dev/null; then
		# C. 国内
		local CODE_NAME=$(lsb_release -cs)
		local COMPONENT="main restricted universe multiverse"
		tee /etc/apt/sources.list >/dev/null<<-EOF
			deb $APT_MIRROR $CODE_NAME $COMPONENT
			deb $APT_MIRROR $CODE_NAME-updates $COMPONENT
			deb $APT_MIRROR $CODE_NAME-backports $COMPONENT
			deb $APT_MIRROR $CODE_NAME-security $COMPONENT
		EOF
	fi
}

function pkg_prepare_install {
	# 安装 ssh 免交互, 编辑文件
	# Tab 自动补全, nc, ping
	apt-get -y update

	until DEBIAN_FRONTEND=noninteractive \
		apt-get -y install sshpass vim \
			bash-completion netcat-openbsd iputils-ping; do
		apt-get -y update
	done
}

function module_install {
	## 1. Bridge
	# [ERROR FileContent-.proc-sys-net-bridge-bridge-nf-call-iptables]:
	# /proc/sys/net/bridge/bridge-nf-call-iptables does not exist
	DEBIAN_FRONTEND=noninteractive \
		apt-get -y install bridge-utils
	tee /etc/modules-load.d/br.conf >/dev/null<<-EOF
		br_netfilter
	EOF

	modprobe br_netfilter

	## 2. 内核支持
	# [ERROR FileContent-.proc-sys-net-ipv4-ip_forward]:
	# /proc/sys/net/ipv4/ip_forward contents are not set to 1
	tee /etc/sysctl.d/k8s.conf >/dev/null<<-EOF
		net.ipv4.ip_forward=1
	EOF

	sysctl -p /etc/sysctl.d/k8s.conf
}

function runtime_install_containerd {
	## 创建镜像仓库文件
	if ! curl --connect-timeout 2 google.com &>/dev/null; then
		# C. 国内
		AURL=${APT_CONTAINERD}
	else
		# A. 国外
		AURL=https://download.docker.com
	fi

	# 运行时仓库地址 ${AURL}/linux/ubuntu
	tee /etc/apt/sources.list.d/docker.list >/dev/null<<-EOF
		deb ${AURL}/linux/ubuntu $(lsb_release -cs) stable
	EOF

	## 导入公钥
	# apt-key is deprecated
	cat /tmp/gpg-docker \
		| gpg --import --no-default-keyring --keyring gnupg-ring:/etc/apt/trusted.gpg.d/docker.gpg
	chmod a+r /etc/apt/trusted.gpg.d/docker.gpg

	# 安装 containerd
	apt-get -y update
	DEBIAN_FRONTEND=noninteractive \
		apt-get -y install containerd.io

	# 生成和更新配置文件 /etc/containerd/config.toml
	containerd config default \
		| sed -e '/SystemdCgroup/s+false+true+' \
			-e "/sandbox_image/s+3.6+3.9+" \
		| tee /etc/containerd/config.toml >/dev/null
	
	if ! curl --connect-timeout 2 google.com &>/dev/null; then
		# C. 国内
		#https://help.aliyun.com/zh/acr/user-guide/accelerate-the-pulls-of-docker-official-images?spm=a2c4g.11186623.0.0.4f941d829QlWW1
		mkdir -p /etc/containerd/certs.d/docker.io

		tee /etc/containerd/certs.d/docker.io/hosts.toml >/dev/null<<-EOF
		server = "https://registry-1.docker.io"
		[host."${DOCKER_IO_MIRROR}"]
		  capabilities = ["pull", "resolve", "push"]
		EOF

		sed -i /etc/containerd/config.toml \
			-e "/sandbox_image/s+registry.k8s.io+${REGISTRY_MIRROR}+" \
			-e '/config_path/s+""+"/etc/containerd/certs.d"+'
	fi

	# 重启 containerd 服务
	systemctl restart containerd
}

function runtime_install_docker {
	## 创建镜像仓库文件
	if ! curl --connect-timeout 2 google.com &>/dev/null; then
		# C. 国内
		local AURL=${APT_CONTAINERD}
	else
		# A. 国外
		local AURL=https://download.docker.com
	fi
	
	# 运行时仓库地址 ${AURL}/linux/ubuntu
	tee /etc/apt/sources.list.d/docker.list >/dev/null<<-EOF
		deb ${AURL}/linux/ubuntu $(lsb_release -cs) stable
	EOF

	## 导入公钥
	# apt-key is deprecated
	cat /tmp/gpg-docker \
		| gpg --import --no-default-keyring --keyring gnupg-ring:/etc/apt/trusted.gpg.d/docker.gpg
	
	chmod a+r /etc/apt/trusted.gpg.d/docker.gpg

	# 安装 containerd
	apt-get -y update
	
	DEBIAN_FRONTEND=noninteractive \
		apt-get -y install docker.io

	# 生成和更新配置文件 /etc/docker/daemon.json
	mkdir -p /etc/docker
	
	tee /etc/docker/daemon.json >/dev/null >/dev/null<<-EOF
		{
		  "exec-opts": ["native.cgroupdriver=systemd"],
		  "log-driver": "json-file",
		  "log-opts": {
		    "max-size": "100m",
		    "max-file": "10"
		  }
		}
	EOF
	
	if ! curl --connect-timeout 2 google.com &>/dev/null; then
		# C. 国内
		sed -i '/exec-opts/i\  "registry-mirrors": ["'${DOCKER_IO_MIRROR}'"],' /etc/docker/daemon.json
	fi

	# 重启 containerd 服务
	systemctl restart docker

	#== cri-dockerd
	# https://kubernetes.io/zh-cn/docs/tasks/administer-cluster/migrating-from-dockershim/migrate-dockershim-dockerd/
	if ! curl --connect-timeout 2 google.com &>/dev/null; then
		# C. 国内
		local CURL=https://hub.gitmirror.com/
	fi
	
	if [ "$(uname -m)" = "aarch64" ]; then 
		local CLI_ARCH=arm64
	else
		local CLI_ARCH=amd64
	fi
	
	local C_VERSION=$(curl -sL https://api.github.com/repos/Mirantis/cri-dockerd/releases/latest | awk '/tag_name/ {print $2}' | sed 's/[",v]//g')

	until curl -#LO ${CURL}https://github.com/Mirantis/cri-dockerd/releases/download/v${C_VERSION}/cri-dockerd_${C_VERSION}.3-0.ubuntu-focal_${CLI_ARCH}.deb; do
		sleep 1
	done
	dpkg -i cri-dockerd_${C_VERSION}.3-0.ubuntu-focal_${CLI_ARCH}.deb
	
	sed -i '/ExecStart/s+$+ --network-plugin=cni --pod-infra-container-image=registry.aliyuncs.com/google_containers/pause:3.9+' \
		/lib/systemd/system/cri-docker.service
	
	systemctl daemon-reload
	systemctl restart cri-docker
}

function k8s_install {
	## https://kubernetes.io/zh-cn/docs/setup/production-environment/tools/kubeadm/install-kubeadm/
	# 1. 更新 apt 包索引并安装使用 Kubernetes apt 仓库所需要的包
	DEBIAN_FRONTEND=noninteractive \
		apt-get -y install apt-transport-https ca-certificates curl gpg
	
	# 2. 下载用于 Kubernetes 软件包仓库的公共签名密钥
	[ ! -d /etc/apt/keyrings ] && mkdir /etc/apt/keyrings
	if ! curl --connect-timeout 2 google.com &>/dev/null; then
		# C. 国内
		local AURL=${APT_K8S_MIRROR}/v"${KUBERNETES_VERSION}"/deb
	else
		# F. 国外
		local AURL=https://pkgs.k8s.io/core:/stable:/v"${KUBERNETES_VERSION}"/deb
	fi
	local KFILE=/etc/apt/keyrings/kubernetes-apt-keyring.gpg
	cat /tmp/gpg-kubernetes | gpg --dearmor -o ${KFILE}
	
	# 3. 添加 Kubernetes apt 仓库
	tee /etc/apt/sources.list.d/kubernetes.list >/dev/null<<-EOF
		deb [signed-by=${KFILE}] ${AURL} /
	EOF
	
	# 4. 更新 apt 包索引
	apt-get -y update
	
	# 列出安装版本的小版本
	local KV=$(apt-cache madison kubeadm | awk -F\| '/'${KUBERNETES_VERSION}.1'/ {print $2}' | sed s+\ ++g)
	
	# 安装 kubeadm、kubelet 和 kubectl
	DEBIAN_FRONTEND=noninteractive \
		apt-get -y install kubeadm=${KV} kubelet=${KV} kubectl=${KV}
	
	# 4. 锁定当前版本
	apt-mark hold kubelet kubeadm kubectl
}

function crictl_config {
	# 生成 crictl 命令的配置文件
	case $RUNTIME in
		containerd)
			crictl config \
			--set runtime-endpoint=unix:///run/containerd/containerd.sock \
			--set image-endpoint=unix:///run/containerd/containerd.sock \
			--set timeout=10
			;;
		docker)
			crictl config \
			--set runtime-endpoint=unix:///var/run/cri-dockerd.sock \
			--set image-endpoint=unix:///var/run/cri-dockerd.sock \
			--set timeout=10
			;;
	esac

	# crictl 命令 Tab 键补全
	source <(crictl completion bash)

	crictl completion bash \
		| tee /etc/bash_completion.d/crictl >/dev/null

	# 普通用户可以直接执行 crictl 命令
	usermod -aG root ${USERNAME}
}


function nerdctl_install {
	if ! curl --connect-timeout 2 google.com &>/dev/null; then
		# C. 国内
		local CURL=https://hub.gitmirror.com/
	fi

	if [ "$(uname -m)" = "aarch64" ]; then 
		local CLI_ARCH=arm64
	else
		local CLI_ARCH=amd64
	fi

	local N_VERSION=$(curl -sL https://api.github.com/repos/containerd/nerdctl/releases/latest | awk '/tag_name/ {print $2}' | sed 's/[",v]//g')

	until curl -#LO ${CURL}https://github.com/containerd/nerdctl/releases/download/v${N_VERSION}/nerdctl-${N_VERSION}-linux-${CLI_ARCH}.tar.gz; do
		sleep 1
	done

	tar xf nerdctl-${N_VERSION}-linux-${CLI_ARCH}.tar.gz -C /usr/local/sbin/
	rm nerdctl-${N_VERSION}-linux-${CLI_ARCH}.tar.gz

	# Generate the autocompletion script
	source <(nerdctl completion bash)

	nerdctl completion bash | tee /etc/bash_completion.d/nerdctl >/dev/null

}

function kubeadm_tab {
	[ ! -d /root/.kube ] && mkdir /root/.kube
	[ ! -d /home/${USERNAME}/.kube ] \
		&& su - ${USERNAME} -c "mkdir /home/${USERNAME}/.kube"
	
	source <(kubeadm completion bash)

	kubeadm completion bash \
		| tee /etc/bash_completion.d/kubeadm >/dev/null
}

function kubeadm_init {
	if ! curl --connect-timeout 2 google.com &>/dev/null; then
		## C. 国内
		# 拉取镜像...
		case $RUNTIME in
			containerd)
				kubeadm config images pull \
				--kubernetes-version ${KUBERNETES_VERSION}.1 \
				--image-repository ${REGISTRY_MIRROR}
				;;
			docker)
				kubeadm config images pull \
				--kubernetes-version ${KUBERNETES_VERSION}.1 \
				--image-repository ${REGISTRY_MIRROR} \
				--cri-socket=unix:///var/run/cri-dockerd.sock
				;;
		esac

		# k8s 集群初始化
		case $RUNTIME in
			containerd)
				kubeadm init \
				--kubernetes-version ${KUBERNETES_VERSION}.1 \
				--apiserver-advertise-address=${CONTROL_IP} \
				--pod-network-cidr=${POD_CIDR} \
				--service-cidr=${SERVICE_CIDR} \
				--node-name=$(hostname -s) \
				--image-repository=${REGISTRY_MIRROR}
				;;
			docker)
				kubeadm init \
				--kubernetes-version ${KUBERNETES_VERSION}.1 \
				--apiserver-advertise-address=${CONTROL_IP} \
				--pod-network-cidr=${POD_CIDR} \
				--service-cidr=${SERVICE_CIDR} \
				--node-name=$(hostname -s) \
				--image-repository=${REGISTRY_MIRROR} \
				--cri-socket=unix:///var/run/cri-dockerd.sock
				;;
		esac
		
	else
		## A. 国外
		# 拉取镜像...
		case $RUNTIME in
			containerd)
				kubeadm config images pull \
				--kubernetes-version ${KUBERNETES_VERSION}.1
				;;
			docker)
				kubeadm config images pull \
				--kubernetes-version ${KUBERNETES_VERSION}.1 \
				--cri-socket=unix:///var/run/cri-dockerd.sock
				;;
		esac
		# k8s 集群初始化
		case $RUNTIME in
			containerd)
				kubeadm init \
				--kubernetes-version ${KUBERNETES_VERSION}.1 \
				--apiserver-advertise-address=${CONTROL_IP} \
				--pod-network-cidr=${POD_CIDR} \
				--service-cidr=${SERVICE_CIDR} \
				--node-name=$(hostname -s)
				;;
			docker)
				kubeadm init \
				--kubernetes-version ${KUBERNETES_VERSION}.1 \
				--apiserver-advertise-address=${CONTROL_IP} \
				--pod-network-cidr=${POD_CIDR} \
				--service-cidr=${SERVICE_CIDR} \
				--node-name=$(hostname -s) \
				--cri-socket=unix:///var/run/cri-dockerd.sock
				;;
		esac
	fi
}

function client_config {
	## - vagrant
	# 为普通用户准备 kubectl 命令配置文件
	cat /etc/kubernetes/admin.conf \
		| su - ${USERNAME} -c "tee /home/${USERNAME}/.kube/config >/dev/null"
	
	## - root
	# 为 root 用户准备 kubectl 命令配置文件
	echo "export KUBECONFIG=/etc/kubernetes/admin.conf" \
		| tee -a /root/.bashrc
	export KUBECONFIG=/etc/kubernetes/admin.conf
}

function kubeadm_join {
	# 正在确认 API SERVER 已启动...
	until nc -w 2 ${CONTROL_IP} 6443; do
		sleep 2
	done
	
	# 加入集群
	case $RUNTIME in
		containerd)
			sudo $(sshpass -p ${ROOT_PASS} ssh root@${VB_MASTER} kubeadm token create --print-join-command)
			;;
		docker)
			sudo $(sshpass -p ${ROOT_PASS} ssh root@${VB_MASTER} kubeadm token create --print-join-command) --cri-socket=unix:///var/run/cri-dockerd.sock
			;;
	esac
	
}

function cni_calico_install {
	# curl https://raw.githubusercontent.com/projectcalico/calico/${CALICO_VERSION}/manifests/calico.yaml -O

	local CFILE=$(curl -s https://docs.tigera.io/calico/latest/getting-started/kubernetes/self-managed-onprem/onpremises | grep -o http.*/calico.yaml | awk '{print $NF}')
	
	if ! curl --connect-timeout 2 google.com &>/dev/null; then
		# C. 国内
		local CURL=$(echo ${CFILE} | sed 's/githubusercontent/gitmirror/')
	else
		# A. 国外
		local CURL=${CFILE}
	fi
	
	## 安装 calico
	# docker.io 无法访问，使用 quay.io
	curl -s $CURL | sed 's+docker.io+quay.io+' | kubectl apply -f-
}

function cni_cilium_install {
  # 1. URL
  if ! curl --connect-timeout 2 google.com &>/dev/null; then
	# C. 国内
	local CURL=https://raw.gitmirror.com
	local DURL=https://hub.gitmirror.com/
  else
	# A. 国外
	local CURL=https://raw.githubusercontent.com
  fi
  
  # 2. ARCH
  if [ "$(uname -m)" = "aarch64" ]; then 
	local CLI_ARCH=arm64
  else
	local CLI_ARCH=amd64
  fi

  # 3. WGET
  local CILIUM_CLI_VERSION=$(curl -s ${CURL}/cilium/cilium-cli/main/stable.txt)

  until curl -s -L --fail --remote-name-all ${DURL}https://github.com/cilium/cilium-cli/releases/download/${CILIUM_CLI_VERSION}/cilium-linux-${CLI_ARCH}.tar.gz; do
	sleep 1
  done
  
  tar xvf cilium-linux-${CLI_ARCH}.tar.gz -C /usr/local/bin
  rm cilium-linux-${CLI_ARCH}.tar.gz

  # 4. INSTALL
  cilium install --wait

  cilium status

  # 5. cilium_TAB
  source <(cilium completion bash)

  cilium completion bash \
  	| tee /etc/bash_completion.d/cilium >/dev/null
}

function client_tab {
	# for kubectl tab 立即生效
	source <(kubectl completion bash)
	
	# for kubectl tab 永久生效
	kubectl completion bash \
		| tee /etc/bash_completion.d/kubectl >/dev/null
}

function client_alias {
	# kubectl alias 立即生效
	alias k='kubectl'
	complete -F __start_kubectl k

	# kubectl alias 永久生效
	tee -a /etc/bash.bashrc >/dev/null<<-EOF
		alias k='kubectl'
		complete -F __start_kubectl k
	EOF
}

function swap_off {
	# 为了保证 kubelet 正常工作,你必须禁用交换分区
	if grep -q swap /etc/fstab; then
		SWAPF=$(awk '/swap/ {print $1}' /etc/fstab | grep -v ^#)
		
		# 关闭交换分区 ${SWAPF}
		swapoff ${SWAPF}
		sed -i '/swap/d' /etc/fstab

		if echo ${SWAPF} 2>/dev/null | grep -q ^UUID; then
			# 存在交换分区 ${SWAPF}
			SWAPP=$(blkid | awk -F: '/'${SWAPF#*=}'/ {print $1}')
			SWAPN=$(echo ${SWAPP} | egrep -o [0-9]*)
			SWAPD=$(echo ${SWAPP} | sed 's/[0-9]//')
			parted ${SWAPD} rm ${SWAPN}
		else
			# 存在交换文件 ${SWAPF}
			# 删除交换文件 ${SWAPF}
			rm ${SWAPF}
		fi
	fi
}

function lv_extend {
	LVN=$(lvdisplay | awk '/Path/ {print $3}')
	if [ ! -z ${LVN} ]; then
		# 查看逻辑卷名称 ${LVN}
		# 扩容逻辑卷 ${LVN}
		lvextend -r -l 100%PVS ${LVN}
	fi
}

function reset_tty1 {
	if pgrep -l agetty &>/dev/null; then
		kill -9 $(pgrep -l agetty | awk '{print $1}')
	fi
}

## Main Area
touch /tmp/k8s-begin-$(date +%y%m%d-%H%M)
# Running `timedatectl set-timezone Asia/Shanghai`...
timezone_change
# Specifies whether root can log in using ssh
passwd_root
# Checks which daemons need to be restarted after library upgrades
service_restart_accept
# Convert the current dynamic IP address to a static IP address
dns_setting
# List of configured APT data sources
apt_mirror && pkg_prepare_install
	# MUST disable swap
	swap_off
	# Add space to a logical volume
	lv_extend
	# Configuring module and kernel for kubeadm preflight
	module_install
	#Installing a container runtime and Configuring a cgroup driver
	case $RUNTIME in
		containerd)
			runtime_install_containerd
			nerdctl_install
			;;
		docker)
			runtime_install_docker
			;;
	esac
		# Installing kubeadm, kubelet and kubectl
		k8s_install
		kubeadm_tab
		crictl_config

case "$(hostname -s)" in
	*master*)
		# Initializes a Kubernetes control-plane node
		kubeadm_init
			# Organizing Cluster Access Using kubeconfig Files
			client_config
				## Deploy a pod network to the cluster
				case $CNI in
					calico)
						cni_calico_install
						;;
					cilium)
						cni_cilium_install
						;;
				esac
			# Set up autocomplete in bash for kubectl
			client_tab
			# use a shorthand alias for kubectl that also works with completion
			client_alias
			reset_tty1
			;;
	*worker*)
		# Initializes a Kubernetes worker node and joins it to the cluster
		kubeadm_join
		reset_tty1
		;;
esac
touch /tmp/k8s-end-$(date +%y%m%d-%H%M)