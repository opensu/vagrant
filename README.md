<img width='100%' src='https://gitlab.com/opensu/vagrant/-/raw/main/images/vagrant-k8s.svg'>





|ID|STEP|<img height='28' src='https://www.svgrepo.com/download/382713/windows-applications.svg'> Windows|<img height='28' src='https://www.svgrepo.com/download/509155/macos.svg'> macOS|<img height='28' src='https://www.svgrepo.com/download/354004/linux-tux.svg'> Linux|
| :-----: | :----------------------------------------------------------: | :----------------------------------------------------------: | :----------------------------------------------------------: | :----------------------------------------------------------: |
|  :one:  |            <img height=42 src='https://www.svgrepo.com/download/454406/software-vmware-workstation.svg'>[VMware](https://support.broadcom.com)            |                    [VMware Workstation]()                    |                      [VMware Fusion]()                       |                    [VMware Workstation]()                    |
|  :two:  |  <img height='24' src='https://www.svgrepo.com/download/354509/vagrant-icon.svg'> [Vagrant](https://developer.hashicorp.com/vagrant/install)  | [AMD64](https://releases.hashicorp.com/vagrant/2.4.1/vagrant_2.4.1_windows_amd64.msi) | [AMD64](https://releases.hashicorp.com/vagrant/2.4.1/vagrant_2.4.1_darwin_amd64.dmg) [ARM64](https://releases.hashicorp.com/vagrant/2.4.1/vagrant_2.4.1_darwin_arm64.dmg) | [X86_64](https://releases.hashicorp.com/vagrant/2.4.0/vagrant-2.4.0-1.x86_64.rpm) [AMD64](https://releases.hashicorp.com/vagrant/2.4.0/vagrant_2.4.0_linux_amd64.zip) |
| :three: | <img height='24' src='https://gitlab.com/opensu/vagrant/-/raw/main/images/vagrant-Providers.svg'>[Vagrant VMware Utility](https://developer.hashicorp.com/vagrant/downloads/vmware) | [AMD64](https://releases.hashicorp.com/vagrant-vmware-utility/1.0.22/vagrant-vmware-utility_1.0.22_windows_amd64.msi) | [AMD64](https://releases.hashicorp.com/vagrant-vmware-utility/1.0.22/vagrant-vmware-utility_1.0.22_darwin_amd64.dmg) [ARM64](https://developer.hashicorp.com/vagrant/docs/providers/vmware/vagrant-vmware-utility) | [X86_64](https://releases.hashicorp.com/vagrant-vmware-utility/1.0.22/vagrant-vmware-utility-1.0.22-1.x86_64.rpm) [AMD64](https://releases.hashicorp.com/vagrant-vmware-utility/1.0.22/vagrant-vmware-utility_1.0.22-1_amd64.deb) |
| :four:  | <img height='24' src='https://gitlab.com/opensu/vagrant/-/raw/main/images/vagrant-Plugins.svg'>[VMware provider plugin](https://developer.hashicorp.com/vagrant/docs/providers/vmware/installation) | <img height=24 src='https://www.svgrepo.com/download/354297/rubygems.svg'>[gem](https://developer.hashicorp.com/vagrant/docs/providers/vmware/installation) |                             同前                             |                             同前                             |
| :five:  | <img height='24' src='https://www.svgrepo.com/download/448226/gitlab.svg'> [Gitlab](https://gitlab.com/opensu/vagrant) |                        `vagrant_k8s`                         |                             同前                             |                             同前                             |

### :one: <img height='24' src='https://www.svgrepo.com/download/354004/linux-tux.svg'> **Linux - RHEL8**

说明：安装相应的依赖包 for VMware

```bash
dnf -y install kernel kernel-headers kernel-devel elfutils-libelf-devel
```

### :three: <img width='28' src='https://www.svgrepo.com/download/509155/macos.svg'> **macOS - ARM64** 只能手动安装 `Vagrant VMware Utility`

​     说明：ARM64 位的系统，官网不提供。<br>
​                 编译好的程序放在瑞通服务器，直接下载使用即可

```bash
#!/bin/bash

export VDIR=/opt/vagrant-vmware-desktop/bin
export VVU=vagrant-vmware-utility
export FSR=https://gitlab.com/opensu/vagrant/-/raw/main/vagrant-vmware-utility

# Create a directory for the executable as root
sudo mkdir -p ${VDIR}

# Download the bin file
sudo curl -# ${FSR}/${VVU} -o ${VDIR}/${VVU}

# Change file modes
sudo chmod +x ${VDIR}/${VVU}

# First, generate the required certificates
sudo ${VDIR}/${VVU} certificate generate

# Finally, install the service
sudo ${VDIR}/${VVU} service install

# Vagrant VMware Utility Service
sudo launchctl load -w /Library/LaunchDaemons/com.vagrant.${VVU}.plist

# Confirm the current status
sudo netstat -an | grep 9922
# EX: tcp4       0      0  127.0.0.1.9922         *.*         LISTEN"
```

### :three: <img height='24' src='https://www.svgrepo.com/download/354004/linux-tux.svg'> **Linux** 需要修改参数 <br>

​	https://github.com/hashicorp/vagrant-vmware-desktop/issues/91#issuecomment-1654532282

```bash
sed -i '/^cmdargs/s+"$+ -license-override professional"+' /etc/init.d/vagrant-vmware-utility
systemctl enable --now vagrant-vmware-utility
reboot
```

### :four: 插件 `VMware provider plugin`

```bash
# >>> China. 国内（使用加速站点）
# https://index.ruby-china.com/gems/vagrant-vmware-desktop
vagrant plugin install vagrant-vmware-desktop --plugin-clean-sources --plugin-source https://gems.ruby-china.com

# >>> Offline. 离线
curl -#LO https://index.ruby-china.com/downloads/vagrant-vmware-desktop-3.0.3.gem
vagrant plugin install vagrant-vmware-desktop-3.0.3.gem --plugin-clean-sources

# >>> Foreign. 国外
vagrant plugin install vagrant-vmware-desktop

# >>> 验证
vagrant plugin list
```

### :five: <img height='24' src='https://www.svgrepo.com/download/448226/gitlab.svg'>下载 [vagrant_k8s](https://gitlab.com/opensu/vagrant.git) 到 `D:\`

​        <img height='18' src='https://www.svgrepo.com/download/382713/windows-applications.svg'> <kbd>Win</kbd> / `cmd` / <kbd>以管理员身份运行</kbd>

```cmd
C:\> vagrant -v											   % vagrant -v
Vagrant 2.4.1

C:\> netstat -an | findstr 9922				 % netstat -an | grep 9922
tcp4       0      0  127.0.0.1.9922         *.*                    LISTEN

C:\> vagrant plugin list							 % vagrant plugin list
vagrant-vmware-desktop (3.0.3, global)
```

<img height=24 src='https://gitlab.com/opensu/vagrant/-/raw/main/images/Vagrant-Cloud.svg'><img height=22 src='https://www.svgrepo.com/download/452122/ubuntu.svg'> box 下载慢。可使用下载软件下载后，添加、并验证

- amd64

  ```bash
  # 下载 系统 模板
  curl -#LO https://app.vagrantup.com/bento/boxes/ubuntu-20.04/versions/202404.23.0/providers/vmware_desktop/amd64/vagrant.box
  # 添加
  vagrant box add bento/ubuntu-20.04 vagrant.box
  
  # 验证
  vagrant box list
  ```
  
- arm64

  ```bash
  curl -#LO https://app.vagrantup.com/bento/boxes/ubuntu-20.04/versions/202404.23.0/providers/vmware_desktop/arm64/vagrant.box
  
  vagrant box add bento/ubuntu-20.04 vagrant.box
  
  vagrant box list
  ```


> <img width='28' src='https://www.svgrepo.com/download/509155/macos.svg'> **macOS** 
>
> - `vagrant up` 第一次使用时，
>   需要执行两遍
>   第一遍会创建 NIC/`vmnet2`

```cmd
切 换盘符
C:\> d:

切换 工作目录
D:\> cd vagrant-k8s

[创建 | 启动] 虚拟机
D:\vagrant-k8s> vagrant up

销毁 虚拟机
D:\vagrant-k8s> vagrant destroy -f

登陆 虚拟机
D:\vagrant-k8s> vagrant ssh

虚拟机 关机
D:\vagrant-k8s> vagrant halt
```