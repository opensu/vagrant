# -*- mode: ruby -*-
# vi: set ft=ruby :

## VARS =begin
#  变量文件
require "yaml"
settings = YAML.load_file "settings.yml"
#  CLONE value. if true SIZE< 1.3GB, if false SIZE> 13GB
CLONE_VALUE = settings["vmware"]["clone"]
#  默认 PROVIDER
VAGRANT_DEFAULT_PROVIDER = "vmware_desktop"
#  取出网段
IP_SECTIONS = settings["network"]["control_ip"].match(/^([0-9.]+\.)([^.]+)$/)
#  网络地址
IP_NW = IP_SECTIONS.captures[0]
#  起始 IP
IP_START = Integer(IP_SECTIONS.captures[1])
#  取出 工作节点个数
NUM_WORKER_NODES = settings["nodes"]["workers"]["count"]
#  虚拟机 NAME
VB_MASTER = settings["nodes"]["control"]["name"]
VB_WORKER = settings["nodes"]["workers"]["name"]
#  虚拟机 DIRECTORY
VMWARE_DIRECTORY = settings["vmware"]["directory"] + "/VM-K8s"
## VARS =end

## Configuration Version =begin
Vagrant.configure("2") do |config|
  ## This configures what box the machine will be brought up against
  #  The value here should be the name of an installed box or a shorthand name of a box in HashiCorp's Vagrant Cloud
  if `uname -m`.strip == "arm64"
    config.vm.box = settings["vmware"]["box"]["arm64"]
    APT_MIRROR = settings["software"]["apt_mirror"]["arm64"]
    GUEST_OS = "arm-ubuntu-64"
  else
    config.vm.box = settings["vmware"]["box"]["amd64"]
    APT_MIRROR = settings["software"]["apt_mirror"]["amd64"]
    GUEST_OS = "ubuntu-64"
  end
  
  ## If true, Vagrant will check for updates to the configured box on every vagrant up
  #  If an update is found, Vagrant will tell the user
  config.vm.box_check_update = true
  
  ## The Vagrant Shell provisioner =begin
  #  allows you to upload and execute a script within the guest machine
  config.vm.provision "shell", 
    env: {
      "IP_NW" => IP_NW, 
      "IP_START" => IP_START, 
      "NUM_WORKER_NODES" => NUM_WORKER_NODES,
      "VB_MASTER" => VB_MASTER,
      "VB_WORKER" => VB_WORKER
    }, 
    inline: <<-SHELL
      # update hosts
      echo "${IP_NW}$((IP_START)) ${VB_MASTER}" >> /etc/hosts
      for i in `seq 1 ${NUM_WORKER_NODES}`; do
        echo "${IP_NW}$((IP_START+i)) ${VB_WORKER}${i}" >> /etc/hosts
      done
      # set default target
      if systemctl get-default | grep -q graphical.target; then
        sudo systemctl isolate multi-user.target
        sudo systemctl set-default multi-user.target
      fi
      # reset_tty1
      if pgrep -l agetty &>/dev/null; then
        kill -9 $(pgrep -l agetty | awk '{print $1}')
      fi
    SHELL

  ## Configures synced folders on the machine, 
  #  so that folders on your host machine can be synced to and from the guest machine
  config.vm.synced_folder ".", "/vagrant", disabled: true
=begin
  if `uname`.strip == "Linux"
    # - Linux
    config.vm.synced_folder ".", "/vagrant", 
      type: "nfs", nfs_version: 4, nfs_udp: false,
        linux__nfs_options: ['rw','no_subtree_check','no_root_squash','async']
  elsif `uname`.strip == "Darwin"
    # - MacOS
    config.vm.synced_folder ".", "/vagrant", 
      type: "nfs"
  else
    # - Windows
    config.vm.synced_folder ".", "/vagrant", 
      type: "smb", smb_username: SMBU, smb_password: SMBP
  end
=end
  ## The Vagrant Shell provisioner =end

  ## Machine Settings master =begin
  config.vm.define VB_MASTER, primary: true do |master|
    ## Configures networks on the machine
    master.vm.network "private_network", ip: settings["network"]["control_ip"]

    ## The hostname the machine should have
    #  If set to a string, the hostname will be set on boot
    #  If set, Vagrant will update /etc/hosts on the guest with the configured hostname
    master.vm.hostname = VB_MASTER

    ## Provider settings =begin
    master.vm.provider VAGRANT_DEFAULT_PROVIDER do |v|
      ## HGFS is functional within the guest
      #  This defaults to detected capability of the guest
      v.functional_hgfs = false
      ## Launch guest with a GUI
      #  This defaults to false
      v.gui = true
      ## Path for storing VMware clones
      #  This defaults to ./.vagrant
      v.clone_directory = VMWARE_DIRECTORY
      ## Use linked clones instead of full copy clones
      v.linked_clone = CLONE_VALUE
      ## VMX key/value pairs to set or unset
      #  If the value is nit, the key will be deleted
      v.vmx["displayName"] = VB_MASTER
      v.vmx["memsize"] = settings["nodes"]["control"]["memory"]
      v.vmx["numvcpus"] = settings["nodes"]["control"]["cpu"]
      v.vmx["annotation"] = settings["nodes"]["annotation"]
      v.vmx["guestOS"] = GUEST_OS
      v.vmx["cpuid.coresPerSocket"] = "1"
      v.vmx["ethernet0.pcislotnumber"] = "160"
      v.vmx["ethernet1.pcislotnumber"] = "224"
    end
    ## Provider settings master =end
    
    ## The Vagrant Shell provisioner =begin
    #  allows you to upload and execute a script within the guest machine
    master.vm.provision "shell",
    env: {
      "DNS_SERVERS" => settings["network"]["dns_servers"].join(" "),
      "USER_PASS" => settings["vmware"]["os"]["user_pass"],
      "ROOT_PASS" => settings["vmware"]["os"]["root_pass"],
      "CONTROL_IP" => settings["network"]["control_ip"],
      "POD_CIDR" => settings["network"]["pod_cidr"],
      "SERVICE_CIDR" => settings["network"]["service_cidr"],
      "KUBERNETES_VERSION" => settings["software"]["kubernetes"],
      "RUNTIME" => settings["software"]["runtime"],
      "CNI" => settings["software"]["cni"],
      "REGISTRY_MIRROR" => settings["software"]["registry_mirror"],
      "APT_MIRROR" => APT_MIRROR,
      "APT_CONTAINERD" => settings["software"]["apt_containerd"],
      "APT_K8S_MIRROR" => settings["software"]["apt_k8s_mirror"],
      "QUAY_IO_MIRROR" => settings["software"]["quay_io_mirror"],
      "DOCKER_IO_MIRROR" => settings["software"]["docker_io_mirror"],
      "K8S_IO_MIRROR" => settings["software"]["k8s_io_mirror"]
      },
      path: "bootstrap.sh"
    ## The Vagrant Shell provisioner =end
  end
  ## Machine Settings master =end

  ## Machine Settings worker =begin
  (1..NUM_WORKER_NODES).each do |i|
    config.vm.define VB_WORKER + "#{i}" do |node|
      ## Configures networks on the machine
      node.vm.network "private_network", ip: IP_NW + "#{IP_START + i}"

      ## The hostname the machine should have
      #  If set to a string, the hostname will be set on boot
      #  If set, Vagrant will update /etc/hosts on the guest with the configured hostname
      node.vm.hostname = VB_WORKER + "#{i}"

      ## Provider settings =begin
      node.vm.provider VAGRANT_DEFAULT_PROVIDER do |v|
        ## HGFS is functional within the guest
        #  This defaults to detected capability of the guest
        v.functional_hgfs = false
        ## Launch guest with a GUI
        #  This defaults to false
        v.gui = true
        ## Path for storing VMware clones
        #  This defaults to ./.vagrant
        v.clone_directory = VMWARE_DIRECTORY
        ## Use linked clones instead of full copy clones
        v.linked_clone = CLONE_VALUE
        ## VMX key/value pairs to set or unset
        #  If the value is nit, the key will be deleted
        v.vmx["displayName"] = VB_WORKER + "#{i}"
        v.vmx["memsize"] = settings["nodes"]["control"]["memory"]
        v.vmx["numvcpus"] = settings["nodes"]["control"]["cpu"]
        v.vmx["annotation"] = settings["nodes"]["annotation"]
        v.vmx["guestOS"] = GUEST_OS
        v.vmx["cpuid.coresPerSocket"] = "1"
        v.vmx["ethernet0.pcislotnumber"] = "160"
        v.vmx["ethernet1.pcislotnumber"] = "224"
      end
      ## Provider settings =end
      
      ## The Vagrant Shell provisioner =begin
      #  allows you to upload and execute a script within the guest machine
      node.vm.provision "shell",
        env: {
          "DNS_SERVERS" => settings["network"]["dns_servers"].join(" "),
          "USER_PASS" => settings["vmware"]["os"]["user_pass"],
          "ROOT_PASS" => settings["vmware"]["os"]["root_pass"],
          "CONTROL_IP" => settings["network"]["control_ip"],
          "VB_MASTER" => VB_MASTER,
          "KUBERNETES_VERSION" => settings["software"]["kubernetes"],
          "RUNTIME" => settings["software"]["runtime"],
          "CNI" => settings["software"]["cni"],
          "REGISTRY_MIRROR" => settings["software"]["registry_mirror"],
          "APT_MIRROR" => APT_MIRROR,
          "APT_CONTAINERD" => settings["software"]["apt_containerd"],
          "APT_K8S_MIRROR" => settings["software"]["apt_k8s_mirror"],
          "QUAY_IO_MIRROR" => settings["software"]["quay_io_mirror"],
          "DOCKER_IO_MIRROR" => settings["software"]["docker_io_mirror"],
          "K8S_IO_MIRROR" => settings["software"]["k8s_io_mirror"]
          },
        path: "bootstrap.sh"
      ## The Vagrant Shell provisioner =end
    end
  end
  ## Machine Settings worker =end

end
## Configuration Version =end